package tests;

import org.junit.Test;
import pages.HomePage;
import pages.SearchPage;

public class FirstSelenideTest extends BaseTest {

    HomePage homePage = new HomePage();

    SearchPage searchPage = new SearchPage();

    private String city = "Анталья";

    private String starsCount = "5";

    // Тест-кейс
    // Зайти на сайт www.booking.com
    // Ввести в поиске "Анталья"
    // Нажать на кнопку "Найти"
    // Проверить, что в поиске отображается "Анталья"
    // Выбрать "5 звезд"
    // Убедиться, что все отели на данной странице имеют "5 звезд"

    @Test()
    public void successBookingFiveStarsTest() {
        homePage.openHomePage()
                .acceptCookies()
                .searchCity(city);

        searchPage.checkCityHeader(city)
                  .closeAccountWindow()
                  .fillHotelCheckboxRate(starsCount)
                  .checkHotelRating(starsCount);
    }
}

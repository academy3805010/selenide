package pages;

import com.codeborne.selenide.CollectionCondition;
import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class SearchPage {

    private final By cityHeader = By.xpath("//h1");

    private final By objectRate = By.xpath("//div[@id='filter_group_class_:r1e:']");

    private final String hotelCheckboxRate = "//input[@name='class=%s']";

    private final String rateInput = "//div[@id='filter_group_class_:r1o:']//input[@name='class=%s']";
    private final String rateInput2 = "//div[@id='filter_group_class_:r1o:']//div[@data-filters-item='class:class=%s']";

    private final By accountMenuCloseButton = By.xpath("//button[@aria-label='Скрыть меню входа в аккаунт.']");

    private final By ratingHotel = By.xpath("//div[@data-testid='rating-stars']");

    private final By star = By.cssSelector("span");

    public SearchPage closeAccountWindow() {
        $(accountMenuCloseButton).shouldBe(Condition.visible).click();
        return this;
    }

    public SearchPage checkCityHeader(String city) {
        $(cityHeader).shouldBe(Condition.visible, Duration.ofSeconds(10))
                     .shouldHave(Condition.text(city));
        return this;
    }

    public SearchPage fillHotelCheckboxRate(String starsCount) {
//        $(objectRate).find(By.xpath(String.format(hotelCheckboxRate, starsCount))).click();
        $(By.xpath(String.format(rateInput2, starsCount)))
                .shouldBe(Condition.visible, Duration.ofSeconds(10))
                .scrollTo()
                .click();
        return this;
    }

    public SearchPage checkHotelRating(String rate) {
        $$(ratingHotel).forEach(x -> x.findAll(star).shouldHave(CollectionCondition.size(Integer.parseInt(rate))));
        return this;
    }
}

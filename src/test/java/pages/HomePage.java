package pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.Selenide;
import org.openqa.selenium.By;

import static com.codeborne.selenide.Selenide.$;
import static tests.BaseTest.CONFIG;

public class HomePage {

    private final By searchField = By.xpath("//input[@class='eb46370fe1']");

    private final By findButton = By.xpath("//button[@type='submit']");

    private final By firstInDropdown = By.xpath("//div[@class='a3332d346a d2f04c9037']");

    private final By cookiesAcceptButton = By.xpath("//button[@id='onetrust-accept-btn-handler']");

    public HomePage openHomePage() {
        Selenide.open(CONFIG.baseUrl());
        return this;
    }

    public HomePage searchCity(String city) {
        $(searchField).sendKeys(city);
        $(firstInDropdown).shouldHave(Condition.text(city));
        $(findButton).click();
        return this;
    }

    public HomePage acceptCookies() {
        $(cookiesAcceptButton).shouldBe(Condition.visible).click();
        return this;
    }
}
